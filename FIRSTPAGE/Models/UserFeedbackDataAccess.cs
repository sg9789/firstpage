﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace FIRSTPAGE.Models
{
    public class UserFeedbackDataAccess : SqlDataAccess
    {
        private static UserFeedbackDataAccess instance = new UserFeedbackDataAccess();

        private UserFeedbackDataAccess() { }

        public static UserFeedbackDataAccess GetInstance
        {
            get
            {
                return instance;
            }
        }

        public int InsertFeedback(UserFeedback theUser)
        {
            int ReturnValue = 0;
            using (SqlCommand InsertCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "FeedbackInsert" })
            {
                ////Usr_Id, Name, image, Email, Phone, Usr_Nm, Pwd
                InsertCommand.Parameters.Add(GetParameter("@ReturnValue", SqlDbType.Int, ReturnValue)).Direction = ParameterDirection.Output;
                InsertCommand.Parameters.Add(GetParameter("@feedback", SqlDbType.VarChar, theUser.feedback));
                InsertCommand.Parameters.Add(GetParameter("@name", SqlDbType.VarChar, theUser.name));
                InsertCommand.Parameters.Add(GetParameter("@email", SqlDbType.VarChar, theUser.email));
                InsertCommand.Parameters.Add(GetParameter("@mob", SqlDbType.VarChar, theUser.mob));

                ExecuteLocalStoredProcedure(InsertCommand);

                ReturnValue = int.Parse(InsertCommand.Parameters[0].Value.ToString());

                return ReturnValue;
            }
        }
        public DataTable SelectFeedBack()
        {
            using (SqlCommand SelectCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "FeedBackSelect" })
            {

                return ExecuteGetDataTable(SelectCommand);
            }
        }
        public DataTable DisableFeedBack(int id)
        {
            using (SqlCommand UpdateCommand = new SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "DisableFeedBack" })
            {
                UpdateCommand.Parameters.Add(GetParameter("@id", SqlDbType.Int, id));

                return ExecuteGetDataTable(UpdateCommand);
            }
        }
    }
}