﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FIRSTPAGE.Models
{
    public class Customer
    {
         public  int id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string mob { get; set; }
        public string Address { get; set; }
        public bool isactive { get; set; }
     
    }
}