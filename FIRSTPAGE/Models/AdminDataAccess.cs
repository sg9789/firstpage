﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace FIRSTPAGE.Models
{
    public class AdminDataAccess: SqlDataAccess
    {
        private static AdminDataAccess instance = new AdminDataAccess();
        private AdminDataAccess()
        {

        }

        public static AdminDataAccess GetInstance
        {
            get{
                return instance;
               }
        }

        // Inserting Login Details
        public DataTable InsertLogin(Admin theLogin)
        {
            using (SqlCommand SelectCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "AdminSelectByLogInId" })
            {
                //SelectCommand.Parameters.Add(GetParameter("@ReturnValue", SqlDbType.Int, ReturnValue)).Direction = ParameterDirection.Output;
                SelectCommand.Parameters.Add(GetParameter("@Usr_Nm", SqlDbType.VarChar, theLogin.Usr_Nm));
                SelectCommand.Parameters.Add(GetParameter("@Pwd", SqlDbType.VarChar, theLogin.Pwd));


                return ExecuteGetDataTable(SelectCommand);
            }
        }


       
        public DataTable SelectAdminbyloginid()
        {
            using (SqlCommand SelectCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "AdminSelectById" })
            {
                SelectCommand.Parameters.Add(GetParameter("@Usr_Id", SqlDbType.Int, Common.LoggedInUser[0].Usr_Id));
                return ExecuteGetDataTable(SelectCommand);
            }
        }
        public int InsertAdmin(Admin theRole)
        {
            int ReturnValue = 0;
            using (SqlCommand InsertCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "AdminInsert" })
            {
                ////Usr_Id, Name, image, Email, Phone, Usr_Nm, Pwd
                InsertCommand.Parameters.Add(GetParameter("@ReturnValue", SqlDbType.Int, ReturnValue)).Direction = ParameterDirection.Output;
                InsertCommand.Parameters.Add(GetParameter("@Name", SqlDbType.VarChar, theRole.Name));
                InsertCommand.Parameters.Add(GetParameter("@Phone", SqlDbType.VarChar, theRole.Phone));
                InsertCommand.Parameters.Add(GetParameter("@Usr_Nm", SqlDbType.VarChar, theRole.Usr_Nm));
                InsertCommand.Parameters.Add(GetParameter("@Email", SqlDbType.VarChar, theRole.Email));
                InsertCommand.Parameters.Add(GetParameter("@AddBy", SqlDbType.Int, Common.LoggedInUser[0].Usr_Id));
                //InsertCommand.Parameters.Add(GetParameter("@Pwd", SqlDbType.VarChar, theRole.Pwd));

                ExecuteLocalStoredProcedure(InsertCommand);

                ReturnValue = int.Parse(InsertCommand.Parameters[0].Value.ToString());

                return ReturnValue;
            }
        }


        public int UpdateAdmin(Admin theRole)
        {
            int ReturnValue = 0;
            using (SqlCommand UpdateCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "AdminUpdate" })
            {
                ////Usr_Id, Name, image, Email, Phone, Usr_Nm, Pwd
                UpdateCommand.Parameters.Add(GetParameter("@ReturnValue", SqlDbType.Int, ReturnValue)).Direction = ParameterDirection.Output;
                UpdateCommand.Parameters.Add(GetParameter("@Usr_Id", SqlDbType.Int, theRole.Usr_Id));
                UpdateCommand.Parameters.Add(GetParameter("@Name", SqlDbType.VarChar, theRole.Name));
                UpdateCommand.Parameters.Add(GetParameter("@Phone", SqlDbType.VarChar, theRole.Phone));
                UpdateCommand.Parameters.Add(GetParameter("@Usr_Nm", SqlDbType.VarChar, theRole.Usr_Nm));
                UpdateCommand.Parameters.Add(GetParameter("@Email", SqlDbType.VarChar, theRole.Email));
                UpdateCommand.Parameters.Add(GetParameter("@ModBy", SqlDbType.Int, Common.LoggedInUser[0].Usr_Id));
                //UpdateCommand.Parameters.Add(GetParameter("@Pwd", SqlDbType.VarChar, theRole.Pwd));

                ExecuteLocalStoredProcedure(UpdateCommand);

                ReturnValue = int.Parse(UpdateCommand.Parameters[0].Value.ToString());

                return ReturnValue;
            }
        }
        public DataTable SelectAdmin()
        {
            using (SqlCommand SelectCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "AdminSelect" })
            {
            
                return ExecuteGetDataTable(SelectCommand);
            }
        }
        public DataTable getbyID(int id)
        {
            using (SqlCommand SelectCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "AdminSelectById" })
            {
                //SelectCommand.Parameters.Add(GetParameter("@ReturnValue", SqlDbType.Int, ReturnValue)).Direction = ParameterDirection.Output;
                SelectCommand.Parameters.Add(GetParameter("@Usr_Id", SqlDbType.Int,id));
                return ExecuteGetDataTable(SelectCommand);
            }
        }
        public DataTable Delete(int ID)
        {
            using (SqlCommand DeleteCommand = new SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "AdminDelete" })
            {
                DeleteCommand.Parameters.Add(GetParameter("@Usr_Id", SqlDbType.Int, ID));

                return ExecuteGetDataTable(DeleteCommand);
            }
        }
        public int ChangePassword(Admin therole)
        {
            int ReturnValue = 0;
            using (SqlCommand UpdateCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "ChangePassword" })
            {
                ////Usr_Id, Name, image, Email, Phone, Usr_Nm, Pwd
                UpdateCommand.Parameters.Add(GetParameter("@ReturnValue", SqlDbType.Int, ReturnValue)).Direction = ParameterDirection.Output;
                UpdateCommand.Parameters.Add(GetParameter("@Usr_Id", SqlDbType.Int, Common.LoggedInUser[0].Usr_Id));
                UpdateCommand.Parameters.Add(GetParameter("@Pwd", SqlDbType.VarChar, therole.Pwd));

                UpdateCommand.Parameters.Add(GetParameter("@ConfmPwd", SqlDbType.VarChar, therole.ConfmPwd));

                ExecuteLocalStoredProcedure(UpdateCommand);

                ReturnValue = int.Parse(UpdateCommand.Parameters[0].Value.ToString());

                return ReturnValue;
            }
        }
        public int ProfilepicUpdate(Admin therole)
        {
            int ReturnValue = 0;
            using (SqlCommand UpdateCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "ChangePassword" })
            {
                ////Usr_Id, Name, image, Email, Phone, Usr_Nm, Pwd
                UpdateCommand.Parameters.Add(GetParameter("@ReturnValue", SqlDbType.Int, ReturnValue)).Direction = ParameterDirection.Output;
                UpdateCommand.Parameters.Add(GetParameter("@Usr_Id", SqlDbType.Int, Common.LoggedInUser[0].Usr_Id));
                UpdateCommand.Parameters.Add(GetParameter("@Pwd", SqlDbType.VarChar, therole.Pwd));

                UpdateCommand.Parameters.Add(GetParameter("@ConfmPwd", SqlDbType.VarChar, therole.ConfmPwd));

                ExecuteLocalStoredProcedure(UpdateCommand);

                ReturnValue = int.Parse(UpdateCommand.Parameters[0].Value.ToString());

                return ReturnValue;
            }
        }

    }
}