﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FIRSTPAGE.Models
{
    public class User
    {
        // id, fname, lname, email, mob, address, sub, description, ipadd

        public int id { get; set; }

        public String fname { get; set; }

        public String lname { get; set; }

        public String email { get; set; }

        public string mob { get; set; }

        public String address { get; set; }

        public String sub { get; set; }

        public String description { get; set; } 
    }
}