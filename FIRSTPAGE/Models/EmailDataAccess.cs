﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace FIRSTPAGE.Models
{
    public class EmailDataAccess : SqlDataAccess
    {
        private static EmailDataAccess instance = new EmailDataAccess();
        private EmailDataAccess()
        {

        }
        public static EmailDataAccess GetInstance
        {
            get
            {
                return instance;
            }
        }
        public int InsertIndivisualEmail(EmailHistory theRole)
        {
            int ReturnValue = 0;
            using (SqlCommand InsertCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "IndivisualEmailInsert" })
            {
                ////Usr_Id, Name, image, Email, Phone, Usr_Nm, Pwd
                InsertCommand.Parameters.Add(GetParameter("@ReturnValue", SqlDbType.Int, ReturnValue)).Direction = ParameterDirection.Output;
                InsertCommand.Parameters.Add(GetParameter("@email", SqlDbType.VarChar, theRole.email));
                InsertCommand.Parameters.Add(GetParameter("@sub", SqlDbType.VarChar, theRole.sub));
                InsertCommand.Parameters.Add(GetParameter("@message", SqlDbType.VarChar, theRole.message));
                ExecuteLocalStoredProcedure(InsertCommand);

                ReturnValue = int.Parse(InsertCommand.Parameters[0].Value.ToString());

                return ReturnValue;
            }
        }
        public DataTable SelectIndivisualMailHistory()
        {
            using (SqlCommand SelectCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "IndivisualEmailSelect" })
            {

                return ExecuteGetDataTable(SelectCommand);
            }
        }
    
            public DataTable UpdateMailVisibility(int id)
            {
                using (SqlCommand UpdateCommand = new SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "EmailResponded" })
                {
                UpdateCommand.Parameters.Add(GetParameter("@id", SqlDbType.Int, id));

                    return ExecuteGetDataTable(UpdateCommand);
                }
            }
        public int InsertCommonEmail(EmailHistory theRole)
        {
            int ReturnValue = 0;
            using (SqlCommand InsertCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "IndivisualEmailInsert" })
            {
                ////Usr_Id, Name, image, Email, Phone, Usr_Nm, Pwd
                InsertCommand.Parameters.Add(GetParameter("@ReturnValue", SqlDbType.Int, ReturnValue)).Direction = ParameterDirection.Output;
                InsertCommand.Parameters.Add(GetParameter("@email", SqlDbType.VarChar, theRole.email));
                InsertCommand.Parameters.Add(GetParameter("@sub", SqlDbType.VarChar, theRole.sub));
                InsertCommand.Parameters.Add(GetParameter("@message", SqlDbType.VarChar, theRole.message));
                ExecuteLocalStoredProcedure(InsertCommand);

                ReturnValue = int.Parse(InsertCommand.Parameters[0].Value.ToString());

                return ReturnValue;
            }
        }
        public DataTable SelectCommonMailHistory()
        {
            using (SqlCommand SelectCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "IndivisualEmailSelect" })
            {

                return ExecuteGetDataTable(SelectCommand);
            }
        }


    }
}