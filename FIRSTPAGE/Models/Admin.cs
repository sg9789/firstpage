﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FIRSTPAGE.Models
{
    public class Admin
    {
        //Usr_Id, Name, image, Email, Phone, Usr_Nm, Pwd, IsActive, IsDelete, AddOn, AddBy, ModOn, ModBy, EmpCode
        public int Usr_Id { get; set; }
        public string Name { get; set; }
        public string image { get; set; }
        public string Phone { get; set; }
        public string Usr_Nm { get; set; }
        public string Pwd { get; set; }
        public string Email { get; set; }
        public string EmpCode { get; set; }
        public string ConfmPwd { get; set; }
    }
}