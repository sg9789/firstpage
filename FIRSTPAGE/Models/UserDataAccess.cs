﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace FIRSTPAGE.Models
{
    public class UserDataAccess : SqlDataAccess
    {
        private static UserDataAccess instance = new UserDataAccess();

        private UserDataAccess() { }

        public static UserDataAccess GetInstance
        {
            get
            {
                return instance;
            }
        }

        public int InsertContact(User theUser)
        {
            int ReturnValue = 0;
            using (SqlCommand InsertCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "Contact_User" })
            {
                ////Usr_Id, Name, image, Email, Phone, Usr_Nm, Pwd
                InsertCommand.Parameters.Add(GetParameter("@ReturnValue", SqlDbType.Int, ReturnValue)).Direction = ParameterDirection.Output;
                InsertCommand.Parameters.Add(GetParameter("@fname", SqlDbType.VarChar, theUser.fname));
                InsertCommand.Parameters.Add(GetParameter("@mob", SqlDbType.VarChar, theUser.mob));
                InsertCommand.Parameters.Add(GetParameter("@email", SqlDbType.VarChar, theUser.email));
                InsertCommand.Parameters.Add(GetParameter("@sub", SqlDbType.VarChar, theUser.sub));

                ExecuteLocalStoredProcedure(InsertCommand);

                ReturnValue = int.Parse(InsertCommand.Parameters[0].Value.ToString());

                return ReturnValue;
            }
        }

         public int UpdateUser(User theRole)
        {
            int ReturnValue = 0;
            using (SqlCommand UpdateCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "User_Create" })
            {
                ////Usr_Id, Name, image, Email, Phone, Usr_Nm, Pwd
                UpdateCommand.Parameters.Add(GetParameter("@ReturnValue", SqlDbType.Int, ReturnValue)).Direction = ParameterDirection.Output;
                UpdateCommand.Parameters.Add(GetParameter("@Usr_Id", SqlDbType.VarChar, theRole.id));
                UpdateCommand.Parameters.Add(GetParameter("@Name", SqlDbType.VarChar, theRole.fname));
                UpdateCommand.Parameters.Add(GetParameter("@Phone", SqlDbType.VarChar, theRole.lname));
                UpdateCommand.Parameters.Add(GetParameter("@Usr_Nm", SqlDbType.VarChar, theRole.fname));
                UpdateCommand.Parameters.Add(GetParameter("@Email", SqlDbType.VarChar, theRole.fname));
                UpdateCommand.Parameters.Add(GetParameter("@AddBy", SqlDbType.Int, 1));
                //UpdateCommand.Parameters.Add(GetParameter("@Pwd", SqlDbType.VarChar, theRole.Pwd));

                ExecuteLocalStoredProcedure(UpdateCommand);

                ReturnValue = int.Parse(UpdateCommand.Parameters[0].Value.ToString());

                return ReturnValue;
            }
        }

        public DataTable getbyID(User obj)
        {
            using (SqlCommand SelectCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "User_SelectById" })
            {
                //SelectCommand.Parameters.Add(GetParameter("@ReturnValue", SqlDbType.Int, ReturnValue)).Direction = ParameterDirection.Output;
                SelectCommand.Parameters.Add(GetParameter("@Usr_Id", SqlDbType.Int, obj.id));
                return ExecuteGetDataTable(SelectCommand);
            }
        }
        public DataTable Delete(string ID)
        {
            using (SqlCommand DeleteCommand = new SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "UserDelete" })
            {
                DeleteCommand.Parameters.Add(GetParameter("@PXMLTABLE", SqlDbType.VarChar, ID));
                return ExecuteGetDataTable(DeleteCommand);
            }
        }

    }
}