﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FIRSTPAGE.Models
{
    public class EmailHistory
    {//id, email, sub, message, sendon, isrespond
        public int id { get; set; }
        public string email { get; set; }
        public string sub { get; set; }
        public string message { get; set; }
        public DateTime sendon { get; set; }
        public bool isrespond { get; set; }
    }
}