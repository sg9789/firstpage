﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FIRSTPAGE.Models
{
    public class UserFeedback
    {
        // id, feedback, ipadd, email, mob

        public int id { get; set; }
        public String name { get; set; }
        public String feedback { get; set; }

        public String ipadd { get; set; }

        public String email { get; set; }

        public String mob { get; set; }
    }
}