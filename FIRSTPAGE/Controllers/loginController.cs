﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FIRSTPAGE.Models;
using System.Data;


namespace FIRSTPAGE.Controllers
{
    public class loginController : Controller
    {
        // GET: login
        int returnvf = 0;
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ForgetPassword()
        {
            return View();
        }
        [HttpGet]
        public JsonResult SelectAdminbyloginid()
        {
            DataTable dt = AdminDataAccess.GetInstance.SelectAdminbyloginid();
            try
            {

                List<Admin> AdminList = new List<Admin>();
                AdminList = (from DataRow dr in dt.Rows
                             select new Admin()
                             {
                                 Usr_Id = Convert.ToInt32(dr["Usr_Id"]),
                                 Name = dr["Name"].ToString(),
                                 Email = dr["Email"].ToString(),
                                 Phone = dr["Phone"].ToString(),
                                 Usr_Nm = dr["Usr_Nm"].ToString(),
                                 Pwd = dr["Pwd"].ToString(),
                                 image = dr["image"].ToString()

                             }).ToList();

                return Json(AdminList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception Ex)
            {
                return Json("could not find any Admin");
            }
        }
        //int ReturnValue = 0;
        protected static class PageVariables
        {
            public static Admin ThisUser
            {
                get
                {
                    Admin TheUser = System.Web.HttpContext.Current.Session["ThisUser"] as Admin;
                    return TheUser;
                }
                set
                {
                    System.Web.HttpContext.Current.Session.Add("ThisUser", value);
                }
            }
        }

        [HttpPost]
        public JsonResult LoginData(Admin theUser)
        {
           try
            {
                DataTable dddttt = new DataTable();
                dddttt = AdminDataAccess.GetInstance.InsertLogin(theUser);
                List<Admin> Userlist = new List<Admin>();
             int usridas = Convert.ToInt32(dddttt.Rows[0]["Usr_Id"]);
               Session["userid"] = usridas;

                if (dddttt.Rows.Count == 1)
                {

                    Userlist = (from DataRow dr in dddttt.Rows
                                select new Admin()
                                {
                                    Usr_Id = Convert.ToInt32(dr["Usr_Id"]),
                                    Usr_Nm = dr["Usr_Nm"].ToString(),
                                    Name = dr["Name"].ToString(),
                                    image= dr["image"].ToString(),
                                    ///EmpCode = dr["EmpCode"].ToString(),
                                    Email = dr["Email"].ToString(),
                                    Phone = dr["Phone"].ToString()
                                }).ToList();

                    Session["LoggedInUser"] = Userlist;
                    PageVariables.ThisUser = Userlist.FirstOrDefault();
                    return Json("User Exists");
                }
                else
                {
                    return Json("User Doesnot Exists.");
                }
            }
            catch(Exception Ex)
            {
                return Json("Password is incorrect");
            }
               
            }
        }
    }
