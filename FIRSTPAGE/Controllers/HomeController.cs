﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FIRSTPAGE.Models;
using System.Net;
using System.Net.Mail;

namespace FIRSTPAGE.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        int returnvf = 0;
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AboutUs()
        {
            return View();
        }
        public ActionResult ROAD_TRAFFIC_SAFETY_SIGNAGE()
        {
            return View();
        }
        public ActionResult Fabrication()
        {
            return View();
        }
        public ActionResult FlexPrinting()
        {
            return View();
        }
        public ActionResult OfficeStationary()
        {
            return View();
        }
        public ActionResult ROAD_SAFETY_EQUIPEMENTS()
        {
            return View();
        }
        public ActionResult PRINTING_PLOTTER_CUTTING()
        {
            return View();
        }
        public ActionResult T_SHIRTS_Printing()
        {
            return View();
        }
        public ActionResult Contact()
        {
            return View();
        }
        public ActionResult mission()
        {
            return View();
        }
        public ActionResult vision()
        {
            return View();
        }
        public ActionResult gallery()
        {
            return View();
        }

        public JsonResult InsertContact(User theUser)
        {
            try
            {
                returnvf = UserDataAccess.GetInstance.InsertContact(theUser);

                if (returnvf > 1)
                {
                    string html = "Dear Sidharth, <br /><br />  Someone trying to contact Please Check the message and give possible response by logging into the application. <br /><br />  thanks & Regards <br /> your support Team";
                    sendEmail(html);
                    return Json("Message Sent successfull we will call you back shortly");
                }
                else if (returnvf == -1)
                {
                    return Json("Contact Records Already Exists.");
                }
                else if (returnvf == -2)
                {
                    return Json("Failed to add Contact Record.");
                }
                else { return Json(""); }

            }
            catch (Exception ex)
            {
                return Json("Records not added," + ex.ToString());
            }
        }

        public JsonResult InsertFeedback(UserFeedback theUser)
        {
            try
            {
                returnvf = UserFeedbackDataAccess.GetInstance.InsertFeedback(theUser);

                if (returnvf > 1)
                {
                    string html = "Dear Sidharth, <br /><br />  Someone given a feedback, Please Check the message in our application by logging in. <br /> <br /> thanks & Regards <br /> your support Team";
                    sendEmail(html);
                    return Json("Thanks for your feedback");
                }
                else if (returnvf == -1)
                {
                    return Json("Feedback you already givened");
                }
                else if (returnvf == -2)
                {
                    return Json("Failed to add Feedback Record.");
                }
                else { return Json(""); }

            }
            catch (Exception ex)
            {
                return Json("Records not added," + ex.ToString());
            }
        }
        public static void sendEmail(string html)
        {
            try
            {
              
                MailMessage mail = new MailMessage();
                mail.Bcc.Add("sg9789@gmail.com");
                mail.Bcc.Add("devloperap@gmail.com");
                mail.To.Add("sidarthdasfst@gmail.com");
        
                mail.Subject = "FIRST PAGE";
              
                mail.Body = html;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "relay-hosting.secureserver.net"; //Or Your SMTP Server Address
                //smtp.Credentials = new System.Net.NetworkCredential
                //    ("abhijitprd@gmail.com", "1092021#Ap"); // ***use valid credentials***
                smtp.Port = 25;
                mail.From = new MailAddress("info@firstpages.in");
                //Or your Smtp Email ID and Password
                smtp.EnableSsl = true;
                smtp.Send(mail);
            }
            catch (Exception ex)
            {
                //print("Exception in sendEmail:" + ex.Message);
            }
        }
    }
}