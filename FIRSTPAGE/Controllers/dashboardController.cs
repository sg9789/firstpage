﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FIRSTPAGE.Models;
using System.Net;
using System.Net.Mail;
using System.Data;

namespace FIRSTPAGE.Controllers
{
    public class dashboardController : Controller
    {
        // GET: dashboard
        // GET: dashboard
        int returnvf = 0;
        public ActionResult Default()
        {
            return View();
        }
        public ActionResult AdminRegistration()
        {
            return View();
        }
        public ActionResult CustomerRegistration()
        {
            return View();
        }

        public ActionResult FeedBack_Details()
        {
            return View();
        }

        public ActionResult SendIndivisualMail()
        {
            return View();
        }
        public ActionResult SendCommonMail()
        {
            return View();
        }
        public ActionResult Myprofile()
        {
            return View();
        }

        public ActionResult ChangePassword()
        {
            return View();
        }
        public ActionResult CallbackRequests()
        {
            return View();
        }
        public JsonResult InsertAdmin(Admin theRole)
        {
            try
            {
                if (theRole.Usr_Id >= 1)
                {
                    returnvf = AdminDataAccess.GetInstance.UpdateAdmin(theRole);
                }
                else
                {
                    returnvf = AdminDataAccess.GetInstance.InsertAdmin(theRole);
                }

                if (returnvf > 1) { return Json("Records added Successfully."); }
                else if (returnvf == -1)
                {
                    return Json("Records Already Exists.");
                }
                else if (returnvf == -2)
                {
                    return Json("Failed to add Record.");
                }
                else { return Json(""); }

            }
            catch (Exception ex)
            {
                return Json("Records not added," + ex.ToString());
            }
        }
        public JsonResult SelectAdmin()
        {
            DataTable dt = AdminDataAccess.GetInstance.SelectAdmin();
            try
            {

                List<Admin> AdminList = new List<Admin>();
                AdminList = (from DataRow dr in dt.Rows
                             select new Admin()
                             {
                                 Usr_Id = Convert.ToInt32(dr["Usr_Id"]),
                                 Name = dr["Name"].ToString(),
                                 Email = dr["Email"].ToString(),
                                 Phone = dr["Phone"].ToString(),
                                 Usr_Nm = dr["Usr_Nm"].ToString(),
                                 Pwd = dr["Pwd"].ToString(),
                                 image = dr["image"].ToString()

                             }).ToList();

                return Json(AdminList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception Ex)
            {
                return Json("could not find any Admin");
            }
        }
        public JsonResult getbyID(int id)
        {
            DataTable dt = AdminDataAccess.GetInstance.getbyID(id);
            // MasterState EmpRepo = new MasterState();
            List<Admin> AdminList = new List<Admin>();
            AdminList = (from DataRow dr in dt.Rows
                         select new Admin()
                         {
                             Usr_Id = Convert.ToInt32(dr["Usr_Id"]),
                             Name = dr["Name"].ToString(),
                             Email = dr["Email"].ToString(),
                             Phone = dr["Phone"].ToString(),
                             Usr_Nm = dr["Usr_Nm"].ToString(),
                             // Pwd = dr["Pwd"].ToString(),
                         }).ToList();
            return Json(AdminList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete(int ID)
        {
            try
            {
                AdminDataAccess.GetInstance.Delete(ID);
                return Json("Deleted Successfully");
            }
            catch (Exception ex)
            {
                return Json("couldn't delete");
            }


        }

        public JsonResult InsertCustomer(Customer theRole)
        {
            try
            {
                if (theRole.id >= 1)
                {
                    returnvf = CustomerDataAccess.GetInstance.UpdateCustomer(theRole);
                }
                else
                {
                    returnvf = CustomerDataAccess.GetInstance.InsertCustomer(theRole);
                }

                if (returnvf > 1) { return Json("Records added Successfully."); }
                else if (returnvf == -1)
                {
                    return Json("Records Already Exists.");
                }
                else if (returnvf == -2)
                {
                    return Json("Failed to add Record.");
                }
                else { return Json(""); }

            }
            catch (Exception ex)
            {
                return Json("Records not added," + ex.ToString());
            }
        }
        public JsonResult SelectCustomer()
        {
            DataTable dt = CustomerDataAccess.GetInstance.SelectCustomer();
            try
            {

                List<Customer> CustomerList = new List<Customer>();
                CustomerList = (from DataRow dr in dt.Rows
                                select new Customer()
                                {
                                    id = Convert.ToInt32(dr["id"]),
                                    Name = dr["Name"].ToString(),
                                    Email = dr["Email"].ToString(),
                                    mob = dr["mob"].ToString(),
                                    Address = dr["Address"].ToString(),
                                    // Pwd = dr["Pwd"].ToString(),
                                }).ToList();
                return Json(CustomerList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception Ex)
            {
                return Json("could not find any Admin");
            }
        }
        public JsonResult getbyCustmerID(int id)
        {
            DataTable dt = CustomerDataAccess.GetInstance.getcustomerbyID(id);
            // MasterState EmpRepo = new MasterState();
            List<Customer> CustomerList = new List<Customer>();
            CustomerList = (from DataRow dr in dt.Rows
                            select new Customer()
                            {
                                id = Convert.ToInt32(dr["id"]),
                                Name = dr["Name"].ToString(),
                                Email = dr["Email"].ToString(),
                                mob = dr["mob"].ToString(),
                                Address = dr["Address"].ToString(),
                                // Pwd = dr["Pwd"].ToString(),
                            }).ToList();
            return Json(CustomerList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteCustomer(int ID)
        {
            try
            {
                CustomerDataAccess.GetInstance.DeleteCustomer(ID);
                return Json("Deleted Successfully");
            }
            catch (Exception ex)
            {
                return Json("couldn't delete");
            }


        }
        

        public JsonResult UpdateMailVisibility(int id)
        {
            try
            {
                EmailDataAccess.GetInstance.UpdateMailVisibility(id);
                return Json("Responded!!");
            }
            catch (Exception ex)
            {
                return Json("couldn't Complete the process");
            }


        }
        public JsonResult InsertIndivisualEmail(EmailHistory therole)
        {
            try
            {

                returnvf = EmailDataAccess.GetInstance.InsertIndivisualEmail(therole);


                if (returnvf >= 1)
                {
                    sendEmail(therole);
                    return Json("Records added Successfully.");
                }
                else if (returnvf == -1)
                {
                    return Json("Records Already Exists.");
                }
                else if (returnvf == -2)
                {
                    return Json("Failed to add Record.");
                }
                else { return Json(""); }

            }
            catch (Exception ex)
            {
                return Json("Records not added," + ex.ToString());
            }
        }

        public JsonResult SelectIndivisualMailHistory()
        {
            DataTable dt = EmailDataAccess.GetInstance.SelectIndivisualMailHistory();
            // MasterState EmpRepo = new MasterState();
            List<EmailHistory> EmailList = new List<EmailHistory>();
            EmailList = (from DataRow dr in dt.Rows
                         select new EmailHistory()
                         {
                             id = Convert.ToInt32(dr["id"]),
                             email = dr["email"].ToString(),
                             sub = dr["sub"].ToString(),
                             message = dr["message"].ToString()
                         }).ToList();
            return Json(EmailList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult InsertCommonEmail(EmailHistory therole)
        {
            try
            {

                returnvf = EmailDataAccess.GetInstance.InsertIndivisualEmail(therole);

                
                if (returnvf > 1)
                {
                    sendEmail(therole);
                    return Json("Records added Successfully.");
                }
                else if (returnvf == -1)
                {
                    return Json("Records Already Exists.");
                }
                else if (returnvf == -2)
                {
                    return Json("Failed to add Record.");
                }
                else { return Json(""); }

            }
            catch (Exception ex)
            {
                return Json("Records not added," + ex.ToString());
            }
        }

        public JsonResult SelectCommonMailHistory()
        {
            DataTable dt = EmailDataAccess.GetInstance.SelectCommonMailHistory();
            // MasterState EmpRepo = new MasterState();
            List<EmailHistory> EmailList = new List<EmailHistory>();
            EmailList = (from DataRow dr in dt.Rows
                         select new EmailHistory()
                         {
                             id = Convert.ToInt32(dr["id"]),
                             sub = dr["sub"].ToString(),
                             message = dr["message"].ToString()
                         }).ToList();
            return Json(EmailList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SelectFeedBackDetails()
        {
            DataTable dt = UserFeedbackDataAccess.GetInstance.SelectFeedBack();
            // MasterState EmpRepo = new MasterState();
            List<UserFeedback> EmailList = new List<UserFeedback>();
            EmailList = (from DataRow dr in dt.Rows
                         select new UserFeedback()
                         {
                             id = Convert.ToInt32(dr["id"]),
                             feedback = dr["feedback"].ToString(),
                             email = dr["email"].ToString(),
                             mob = dr["mob"].ToString()
                         }).ToList();
            return Json(EmailList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DisableFeedBack(int id)
        {
            try
            {
                UserFeedbackDataAccess.GetInstance.DisableFeedBack(id);
                return Json("Disabled!!");
            }
            catch (Exception ex)
            {
                return Json("couldn't Complete the process");
            }


        }
        public JsonResult respondedcustomer(int id)
        {
            try
            {
                CustomerDataAccess.GetInstance.respondedcustomer(id);
                return Json("Responded!!");
            }
            catch (Exception ex)
            {
                return Json("couldn't Complete the process");
            }


        }
        
        public JsonResult SelectContact()
        {
            DataTable dt = CustomerDataAccess.GetInstance.SelectContact();
            // MasterState EmpRepo = new MasterState();
            List<User> EmailList = new List<User>();
            EmailList = (from DataRow dr in dt.Rows
                         select new User()
                         {
                             id = Convert.ToInt32(dr["id"]),
                             fname = dr["fname"].ToString(),
                             email = dr["email"].ToString(),
                             mob = dr["mob"].ToString(),
                             sub = dr["sub"].ToString()
                         }).ToList();
            return Json(EmailList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult logout()
        {
            try
            {
                Session.Abandon();
                return RedirectToAction("index", new { Controller = "login" });
            }
            catch (Exception Ex)
            {
                return Json("Please Try for login first");
            }
        }

        public static void sendEmail(EmailHistory email)
        {
            try
            {
                MailMessage mail = new MailMessage();
                if(email.email != null)
                {
                    mail.To.Add(email.email);
                    string message = email.message.ToString();
                    string name = email.email.ToString();
           

                    mail.Body = "Dear, " + name + ", <br /><br /> " + message + " <br /> <br /> thanks & Regards <br /> Shidharth <br /> Director ";
                }
                else
                {
                    DataTable dt = CustomerDataAccess.GetInstance.SelectCustomer();
                    int count = dt.Rows.Count;
                    for (int i=1; i < count; i++)
                    {
                       
                        string message = email.message.ToString();
                        string name = dt.Rows[i]["Name"].ToString();
                        mail.To.Add(dt.Rows[i]["Email"].ToString());

                        mail.Body = "Dear, " +name + ", <br /><br /> " + message + " <br /> <br /> thanks & Regards <br /> Sidharth";
                    }
                    
                }
                mail.From = new MailAddress("info@firstpages.in");
                mail.Bcc.Add("devloperap@gmail.com");
                // mail.To.Add("test@gmail.com");
                //mail.From = new MailAddress("abhijitprd@gmail.com");
                mail.Subject = "FirstPage";
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "relay-hosting.secureserver.net"; //Or Your SMTP Server Address
                //smtp.Credentials = new System.Net.NetworkCredential
                //    ("abhijitprd@gmail.com", "1092021#Ap"); // ***use valid credentials***
                smtp.Port = 25;
                //Or your Smtp Email ID and Password
                smtp.EnableSsl = true;
                smtp.Send(mail);
            }
            catch (Exception ex)
            {
                //print("Exception in sendEmail:" + ex.Message);
            }
        }
        public JsonResult ChangesPassword(Admin therole)
        {
            try
            {
  
                 returnvf = AdminDataAccess.GetInstance.ChangePassword(therole);
                DataTable dt = AdminDataAccess.GetInstance.getbyID(Common.LoggedInUser[0].Usr_Id);
             

                EmailHistory email = new EmailHistory();

                email.email = dt.Rows[0]["Email"].ToString();
                email.message = "Your Password Has Changed SuccessFull, Your Changed New Password is " + therole.ConfmPwd + "";

                if (returnvf ==1)
                {
                    sendEmail(email);
                    return Json("Password Changed Successfully.");
                }
                else if (returnvf == -1)
                {
                    return Json("Incorrect Old Password you are entering");
                }
                else if (returnvf == -2)
                {
                    return Json("Failed to add ");
                }
                else { return Json(""); }

            }
            catch (Exception ex)
            {
                return Json("Password Couldn't changed," + ex.ToString());
            }
        }
        public JsonResult ProfilepicUpdate(Admin therole)
        {
            try
            {

                returnvf = AdminDataAccess.GetInstance.ProfilepicUpdate(therole);
              

                if (returnvf == 1)
                {
                    
                    return Json("Profile Pic Updated Successfully.");
                }
                else if (returnvf == -1)
                {
                    return Json("Couodn't Saved");
                }
                else if (returnvf == -2)
                {
                    return Json("Failed to add ");
                }
                else { return Json(""); }

            }
            catch (Exception ex)
            {
                return Json("Password Couldn't changed," + ex.ToString());
            }
        }
    }
}